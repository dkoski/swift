/*:

[Previous](@previous) [Next](@next)

# Values
[documentation](https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/ClassesAndStructures.html#//apple_ref/doc/uid/TP40014097-CH13-ID82)

Swift has something called "value types".  Other languages have something like this too, but many times it is part of the language, not something that you can define yourself.  Let's look at a familiar example:

```
var a = 10

// b is a copy of a
var b = a

// if we modify a we do not affect b
a = 777

// this will still print 10
print(b)
```

In Swift you can do this with your own types using `struct`:
*/

struct Point : CustomStringConvertible {
	var x, y: Double
	
	init(_ x: Double, _ y: Double) {
		self.x = x
		self.y = y
	}
	
	var description: String {
        // note that you can omit return if it is the only statement
		"\(x), \(y)"
	}
}

//: Now we can assign them, copy them and mutate them:
var p1 = Point(10, 10)
var p2 = p1

p1.x = 50
p2.y = 20

//: If you have a method that mutates the structure you have to declare it as `mutating`.  You won't be able to mutate a `let` variable, only a `var` variable.

struct Dog {
	let name: String
	private(set) var age: Int
	
	init(_ name: String) {
		self.name = name
		age = 0
	}
	
	mutating func haveBirthday() {
		age = age + 1
	}
}

let puppy = Dog("Tiny")
//: This will be an error because the puppy *value* is a `let` variable.  If `Dog` was a class instead of a struct, this would work -- we are not mutating the object reference, we would jus tbe mutating the object itself.
//puppy.haveBirthday()

var pepsi = Dog("Pepsi")
pepsi.haveBirthday()
pepsi.haveBirthday()
pepsi.age

//: The final thing to know is that String, Array, Dictionary (Map), and Set are all *value* types.  In Java, only String is a value type -- and it isn't really, but immutable Strings behave the same way as a value type.

let array1 = [ 10, 20, 30 ]
var array2 = array1
array2.append(40)
//: Since array2 is a value type, that means that array1 still has the same contents.
array1

//: [Previous](@previous) [Next](@next)
