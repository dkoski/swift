/*:

[Previous](@previous) [Next](@next)

# Functional
[documentation](https://useyourloaf.com/blog/swift-guide-to-map-filter-reduce/)

Many languages use what is called an *imperative* style of programming:  you tell the computer how to do something very explicitly.  For example, if you want to take an array and make a new array with the square of the original values you could do something like this:
*/
let input = Array(0 ..< 10)

var result1 = [Int]()
for i in input {
	result1.append(i * i)
}
print(result1)
//: This works fine, but you can also use *functional programming* where you describe what you want done by using functions.  There are a number of additional [benefits](https://en.wikipedia.org/wiki/Functional_programming) and some people find it easier to read.
let result2 = input.map { i in i * i }
print(result2)
//: This says to map (like function composition) the values through this anonymous function (called a [*closure*](https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/Closures.html#//apple_ref/doc/uid/TP40014097-CH11-ID94)) and produce a new array with the values.  You can even chain these together.  What does this do?
let result3 = input.filter { i in i % 2 == 0 }.map { i in i * i }
print(result3)

//: You can make variables that are closures or use functions as closures

// here is a value that can act as a function or a closure
let doubler = { (v: Int) in v * 2 }

// a function can also act as a closure
func tripler(v: Int) -> Int {
    v * 3
}

print(input.map(doubler))
print(input.map(tripler))

//: You can make you own functions that take closures.  The type looks like `(Int) -> Int` (function taking Int and returning Int).

func process(values: [Int], _ apply: (Int) -> Int) -> [Int] {
    var result = [Int]()
    for v in values {
        result.append(apply(v))
    }
    return result
}

print(process(values: input, doubler))

//: [Previous](@previous) [Next](@next)
