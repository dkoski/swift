/*:

[Previous](@previous) [Next](@next)

# Classes
documentation on [classes](https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/ClassesAndStructures.html#//apple_ref/doc/uid/TP40014097-CH13-ID82), [properties](https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/Properties.html#//apple_ref/doc/uid/TP40014097-CH14-ID254), and [methods](https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/Methods.html#//apple_ref/doc/uid/TP40014097-CH15-ID234)

Classes in Swift are similar to classes in other languages.  They have a name, instance variables (called properties in Swift) and methods.

Here is a simple counter class:
*/
class Counter {
	
	// a property (instance variable(
	var count = 0

	// a method
	func increment() {
		count = count + 1
	}
}
//: Now we can construct one and call methods on it:
let c1 = Counter()

c1.count
c1.increment()
c1.count
//: Note that `c1` is a *reference* to an instance of Counter.  We can have several variables reference the same instance:
let c2 = c1

c2.increment()
c1.count
c2.count
//: You can create a class hierarchy by extending one class with another:
class DoubleCounter : Counter {
	// we will override the increment method to count by 2 instead of 1
	override func increment() {
		count = count + 2
	}
}

let c3 = DoubleCounter()

c3.count
c3.increment()
c3.count
//: We do have a problem with our Counter -- callers can modify the count directly!
c3.count = 7
c3.count

//: We can fix that by declaring our `count` as private, but reading the value is pretty handy.  We can also declare the `count` as having a private setter:
class PrivateCount {
	
	private(set) var count = 0
	
	func increment() {
		count = count + 1
	}
}

let pc1 = PrivateCount()

pc1.count
pc1.increment()
pc1.count

//: This would be an error:
// pc1.count = 5
//: Our classes can have initializers (similar to constructor in java) too:
class IncrementCounter {
	// this is declared as a "let" -- that means that once it is set, it cannot change
	let by: Int
	
	private(set) var count = 0

	// this is the in
	init(initial: Int = 0, by: Int = 5) {
		count = initial
		self.by = by
	}
	
	func increment() {
		count = count + by
	}
}

// this will use the default value in the init method
let pc2 = IncrementCounter()

pc2.count
pc2.increment()
pc2.count

//: [Previous](@previous) [Next](@next)
