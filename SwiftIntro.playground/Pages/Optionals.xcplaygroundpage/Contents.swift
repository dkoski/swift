/*:

[Previous](@previous) [Next](@next)

# Optionals
[documentation](https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/TheBasics.html#//apple_ref/doc/uid/TP40014097-CH5-ID330)

Have you ever had to check for a `null` value in another language?  Did you ever forget and get a `NullPointerException`?  *Optionals* are a way to deal with this in the language.  It is a way to describe if a value *can* be null and will force you to check.

For example, if you have a dictionary and you look up a value in it, the result of that lookup would be *optional*.  It might have the value, it might not:
*/
let dictionary = [ "simon" : 12, "david" : 46 ]
//: The value here might not be found.  In this case `"amy"` is not in the dictionary and this will return `nil` (the equivalent of null):
let value = dictionary["amy"]
//: What is the type of `value`?  It isn't `Int`, it is `Int?` -- read that as *optional Int*.  If you want to use the value you have to *unwrap* it.  There are a couple of ways to do it.  You can use `??` to provide a value if it is nil:
print(value ?? 37)
//: You can use `if let` to unwrap it inside an if:
if let nonNilValue = value {
	// I have to use nonNilValue here, not value!
	print(nonNilValue * 2)
}
//: Idiomatically you would probably write that like this:
if let value = value {
	print(value * 2)
}
//: This might seem tricky at first, but it makes programming in Swift a lot less error prone!

//: [Previous](@previous) [Next](@next)
