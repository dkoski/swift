/*:
[Next](@next)

# Variables
[documentation](https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/TheBasics.html#//apple_ref/doc/uid/TP40014097-CH5-ID309)

Variables in Swift are similar to variables in Java or any other language, but there are a few differences:

* you don't need to specify the type if you initialize the variable
* you can use `let` for constants and `var` for variables
* it can represent *optional* values which may or may not be set

*/

// this is a constant value (java: final int x = 10)
let x = 10

// this would be an error
// x = 12

// this is a variable (java: int y = 20, or perhaps Integer y = 20)
var y = 20

// you can assign new values to a var
y = 25
y = y + 5 + x

// unlike java, all Swift types are like "objects".  notice that method calls usually have labels on the arguments
y.advanced(by: 20)

// properties can be computed or stored and are accessed like this (java: y.nonZeroBitCount() )
y.nonzeroBitCount

// if you need to declare a variable with a type you can do it like this
var str: String
str = "dog" + "cat"

// you can print values like this
print(x, y)

// or this
print("x = \(x), y = \(y)")
//: [Next](@next)
