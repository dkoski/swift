/*:

[Previous](@previous) [Next](@next)

# Collection Types
[documentation](https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/CollectionTypes.html#//apple_ref/doc/uid/TP40014097-CH8-ID105)

Swift has the usual collection types: arrays, dictionaries (maps) and sets.

You can use array, dictionary and set *literals* like this:
*/
let array1 = [ 10, 20, 30 ]
let dictionary1 = [ "david" : 46, "simon": 12 ]
let set1: Set = [ 5, 6, 7, 5, 7 ]
//: You use these in the typical way:
array1[1]
dictionary1["simon"]
set1.contains(11)
//: If you want to declare variables of these types, they go like this:
var array2 = [String]()
array2.append("blah")
array2

var dictionary2 = [Int:String]()
dictionary2[10] = "amy"
dictionary2[12] = "simon"
dictionary2

var set2 = Set<String>()
set2.insert("dog")
set2.insert("cat")
set2
//: As an added bonus, Swift sets have useful set operations, unlike some other languages!
let set3: Set = [ "horse", "cat" ]
//: This type creates a new Set and returns it
let set4 = set2.union(set3)
//: And this modifies the value
set2.formUnion(set3)
//: [Previous](@previous) [Next](@next)
