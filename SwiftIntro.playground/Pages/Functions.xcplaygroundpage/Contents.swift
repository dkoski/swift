/*:

[Previous](@previous) [Next](@next)

# Functions
[documentation](https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/Functions.html#//apple_ref/doc/uid/TP40014097-CH10-ID158)

Functions in Swift look like this:

```
func square(value: Int) -> Int {
	return value * value
}
```

This function takes an Int and returns a new Int.

The `value:` in the parameter above names both the input parameter and the label in the method call.  We can rename the parameter like this:
*/
func cube(theValue value: Int) -> Int {
	return value * value * value
}

let x = cube(theValue: 10)
//: Or we can suppress the label with an underscore:
func average(_ first: Int, _ second: Int) -> Double {
	return Double(first + second) / 2.0
}

let a = average(23, 76)
//:If the function doesn't return any value you can omit the `-> Type`
func say(phrase: String) {
	print("I say, \(phrase).")
}

say(phrase: "hello")
//: You can supply default values to arguments as well:
func timesing(value: Int, by: Int = 5) -> Int {
	return value * by
}

timesing(value: 10)
timesing(value: 10, by: 3)

//: [Previous](@previous) [Next](@next)
