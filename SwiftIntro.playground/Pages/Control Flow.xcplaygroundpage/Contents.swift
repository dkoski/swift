/*:

[Previous](@previous) [Next](@next)

# Control Flow
[documentation](https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/ControlFlow.html#//apple_ref/doc/uid/TP40014097-CH9-ID120)

Swift has the usual *control flow* keywords that you would expect.  The biggest difference between Swift and Java is going to be the lack of parenthesis:

```
if condition {
	do something
}
```

There are also cases where you might use *functional programming* techniques to perform operations that might have used for loops in other languages.

Let's look at `if` statements first:
*/
let v = 7
if v % 2 == 0 {
	print("even")
} else {
	print("odd")
}
//: `switch` statements are a little fancier than in other languages.  Note that you do not need a `break` statement to keep it from falling through to the next case.  There is a `fallthrough` keyword if you want it to fall through.
switch v {
case -1000 ..< 0:
	print("negative")
case 0 ... 10:
	print("small")
case 37, 64, 99:
	print("interesting")
case 10 ... 1000:
	print("big")
default:
	print("huge")
}
//: Now let's look at `for` loops:
for i in 0 ..< 10 {
	// iterates 0 1 2 3 4 5 6 7 8 9
	print(i)
}

for i in 0 ... 10 {
	// iterates 0 1 2 3 4 5 6 7 8 9 10
	print(i)
}

let list = [ "cat", "dog", "duck" ]
for i in list {
	print(i)
}

let map = [ "simon" : 12, "david" : 46 ]
for (key, value) in map {
	print(key, value)
}
//: Loops have the usual `continue` and `break` keywords:
for i in list {
	if i.starts(with: "d") {
		continue
	}
	print(i)
}
//: You can also use a `where` to filter the values you iterate:
for i in list where !i.starts(with: "d") {
	print(i)
}
//: `while` loops are similar:
var c = 4
while c > 0 {
	print(c)
	c = c - 1
}
//: You can also execute the test at the end:
c = 0
repeat {
	print(c)
	c = c - 1
} while c > 0
//: [Previous](@previous) [Next](@next)
