/*:

[Previous](@previous) [Next](@next)

# Protocols
[documentation](https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/Protocols.html#//apple_ref/doc/uid/TP40014097-CH25-ID267)

Protocols in Swift are similar to interfaces in Java:  they define a set of properties and methods that the type must implement.  For example, this protocol is part of the Swift standard library:

```
public protocol CustomStringConvertible {
	
	public var description: String { get }
}
```

and it provides a way for a type to print itself out -- the same as `toString()` in Java.  Let's add that to one of our counters:
*/

class IncrementCounter : CustomStringConvertible {
	// this is declared as a "let" -- that means that once it is set, it cannot change
	let by: Int
	
	private(set) var count = 0
	
	// this is the in
	init(initial: Int = 0, by: Int = 5) {
		count = initial
		self.by = by
	}
	
	func increment() {
		count = count + by
	}
//: Here is where we conform to the protocol:
	var description: String {
		return "\(count)"
	}
}

let c1 = IncrementCounter()

print(c1.count)
c1.increment()
//: Now we can simply print the object and it will call the `description` property.
print(c1)

//: [Previous](@previous) [Next](@next)
