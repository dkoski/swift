# Swift Projects

Some projects that we can use to have fun programming in Swift.

## Useful Links

- [Swift Tour](https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/GuidedTour.html#//apple_ref/doc/uid/TP40014097-CH2-ID1)
- [Swift Language Guide](https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/TheBasics.html#//apple_ref/doc/uid/TP40014097-CH5-ID309)
- [Swift Standard Library](https://developer.apple.com/documentation/swift)
- [Swift eBook](https://itunes.apple.com/us/book/the-swift-programming-language/id881256329?mt=11)
- [Everyone Can Code](https://www.apple.com/everyone-can-code/)

